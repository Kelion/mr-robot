import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Teste {

    @Test
    public void emailDelete() throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        boolean fica = true;
        WebElement webElement;


        driver.get("https://webmail.infomedbenner.com.br/");

        driver.findElement(By.id("username")).sendKeys("kelion.aquino@infomedbenner.com.br");
        driver.findElement(By.id("password")).sendKeys("@qwe123!");
        driver.findElement(By.id("password")).submit();

        WebDriverWait wait = new WebDriverWait(driver, TimeUnit.SECONDS.toSeconds(10));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Mail Delivery System']")));
        System.out.println(driver.findElement(By.xpath("//span[text()='Mail Delivery System']")).getText());

        while (fica){

            if(driver.findElements(By.xpath("//span[text()='Mail Delivery System']")).size() != 0){

                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Mail Delivery System']")));
                driver.findElement(By.xpath("//span[text()='Mail Delivery System']")).click();
                driver.findElement(By.xpath("//td[text()='Apagar']")).click();
                Thread.sleep(500);

            }else {
                fica = false;
            }
        }

        driver.quit();
    }
}
